-module(server).
-export([main/0]).

main() ->
    application:start(chumak),
    {ok, Socket} = chumak:socket(rep, "my-rep"),
    chumak:bind(Socket, tcp, "localhost", 8888),            
    loop(Socket).

loop(Socket) ->
    {ok, Input} = chumak:recv(Socket),
    if 
	Input == <<"send">> ->
		chumak:send(Socket, <<"Text">>);
	Input /= <<"send">> ->
                chumak:send(Socket, <<"Wrong command">>)
    end,
    io:format("Q: ~p\n", [Input]),
    loop(Socket).
