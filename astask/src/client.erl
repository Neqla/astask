-module(client).
-export([main/0]).

main() ->
    application:start(chumak),
    {ok, Socket} = chumak:socket(req, "my-req"),
    chumak:connect(Socket, tcp, "localhost", 8888),
    send_messages(Socket, []).

send_messages(Socket, []) ->
   {ok, Command}=io:fread("Command> ", "~s"),
   send_messages(Socket, Command);
   %send_messages(Socket, [<<"send">>]);

send_messages(Socket, [Message|Messages]) ->
    chumak:send(Socket, Message), 
    io:format("Send: ~p\n", [Message]),
    {ok, RecvMessage} = chumak:recv(Socket),
    io:format("Recv: ~p\n", [RecvMessage]),
    send_messages(Socket, Messages).
