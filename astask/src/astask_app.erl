%%%-------------------------------------------------------------------
%% @doc astask public API
%% @end
%%%-------------------------------------------------------------------

-module(astask_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    astask_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
